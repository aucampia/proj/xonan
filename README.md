# extensions for conan

Some of the extra tooling:

- `xonan lenv`:
  This allows you to run commands inside an environment constructed from a local conanfile.
  For example, if you put openssl in your conanfile, then you can use `xonan lenv exec -- openssl` to run the openssl from the environment.
- `xonan senv`:
  This allows you to run commands inside an environment constructed from a shared conan file.
  For example, if you put openssl in your conanfile, then you can use `xonan senv exec -- openssl` to run the openssl from the environment.

## Instructions

### Installation

```bash
pip3 install --user --upgrade git+https://gitlab.com/aucampia/proj/xonan.git@master#egg=xonan
# or
pipx install git+https://gitlab.com/aucampia/proj/xonan.git@master#egg=xonan
```

### Local environment usage

Create a directory:

```bash
mkdir -vp /var/tmp/xonan-demo
cd /var/tmp/xonan-demo
```

Create a conanfile:

```bash
cat > conanfile.txt <<EOF
# https://docs.conan.io/en/latest/reference/conanfile_txt.html
[requires]
openssl/1.1.1h
EOF
```

Install and use the environment:

```bash
# install the environment
xonan lenv install
# run openssl inside the environment
xonan lenv exec -- openssl version
# run bash inside the environment
xonan lenv exec -- bash
# show the environment
xonan lenv env
```

### Shared environment usage

```bash
## create environment directory
mkdir -vp $(xonan senv -n default info | jq -r .config_dir)

cat > "$(xonan senv -n default info | jq -r .conanfile)" <<EOF
# https://docs.conan.io/en/latest/reference/conanfile_txt.html
[requires]
openssl/1.1.1h
EOF

# get info about the environment
xonan senv -n default info

# edit the environment's conanfile
xonan senv -n default edit

# install the environment
xonan senv -n default install

# run a command inside the environment
xonan senv -n default exec -i -- openssl version

# get environment variables of the environment
xonan senv -n default env -i

# source environment variables of the environment
source <(xonan senv -n default env -i)
```

To enable the shared environment for all shells the following can be added to `~/.bashrc`

```bash
source <(xonan senv -n default env)
# or
source ~/.config/xonan-1.0/default/env.sh
```

### Setup JVM dev env

Add remote with JVM packages:

```bash
conan remote add gl-aucampia-conan-pkgs https://gitlab.com/api/v4/projects/27204130/packages/conan
# see https://gitlab.com/aucampia/proj/conan-pkgs/-/packages
```

Setup shared conan env:

```bash
# create env dir
mkdir -vp $(xonan senv -n jvm info | jq -r .config_dir)

# setup env conanfile
cat > "$(xonan senv -n jvm info | jq -r .conanfile)" <<EOF
# https://docs.conan.io/en/latest/reference/conanfile_txt.html
[requires]
openjdk-prebuilt/11.0.11.9@aucampia/default
scala-prebuilt/3.0.0@aucampia/default
gradle-prebuilt/7.0.2@aucampia/default
EOF

# install env
xonan senv -n jvm install
```

Add this to `~/.bashrc`:

```bash
source ~/.config/xonan-1.0/default/env.sh
```

Open a new shell and run java:

```bash
which java
java -version
```

output:

```
$ which java
~/.conan/data/openjdk-prebuilt/11.0.11.9/aucampia/default/package/fde577f178ad15e7948ef4c8ffe4eb6b67a0e0ab/bin/java
$ java -version
openjdk version "11.0.11" 2021-04-20
OpenJDK Runtime Environment AdoptOpenJDK-11.0.11+9 (build 11.0.11+9)
OpenJDK 64-Bit Server VM AdoptOpenJDK-11.0.11+9 (build 11.0.11+9, mixed mode)
```

## TODO

* [ ] Adding shared environments to .bashrc
* [ ] Updating conanfiles from cli

## ...


```bash
xonan senv -n default env -i > "$(xonan senv -n default info | jq -r .config_dir)/env"
~/.config/xonan-1.0/default/env
```
