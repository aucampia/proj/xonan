# SPDX-FileCopyrightText: 2021 Iwan Aucamp
#
# SPDX-License-Identifier: Apache-2.0

import logging
import pathlib
import typing

import click

# from .cli import cli
from .helpers import ContextHelper, InstallMode

LOGGER = logging.getLogger(__name__)


@click.group(name="lenv")
@click.option("--directory", "-C", "directory", type=str)
@click.pass_context
def cli_lenv(ctx: click.Context, directory: str) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )


@cli_lenv.command(name="exec")
@click.option("--install-mode", "-i", type=click.Choice(e.value for e in InstallMode))
@click.argument("args", nargs=-1)
@click.pass_context
def cli_lenv_exec(
    ctx: click.Context,
    install_mode: str,
    args: typing.List[str],
) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    assert ctx.parent is not None
    spec_dir = ctx.parent.params.get("directory")
    use_dir = pathlib.Path(spec_dir) if spec_dir is not None else pathlib.Path.cwd()
    context_helper = ContextHelper(use_dir)
    context_helper.exec(install_mode=InstallMode(install_mode), args=args)


@cli_lenv.command(name="install")
@click.pass_context
def cli_lenv_install(ctx: click.Context) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    assert ctx.parent is not None
    spec_dir = ctx.parent.params.get("directory")
    use_dir = pathlib.Path(spec_dir) if spec_dir is not None else pathlib.Path.cwd()
    context_helper = ContextHelper(use_dir)
    context_helper.install()


@cli_lenv.command(name="env")
@click.option("--install-mode", "-i", type=click.Choice(e.value for e in InstallMode))
@click.argument("args", nargs=-1)
@click.pass_context
def cli_lenv_env(
    ctx: click.Context,
    install_mode: str,
) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    assert ctx.parent is not None
    spec_dir = ctx.parent.params.get("directory")
    use_dir = pathlib.Path(spec_dir) if spec_dir is not None else pathlib.Path.cwd()
    context_helper = ContextHelper(use_dir)
    context_helper.show_shenv(install_mode=InstallMode(install_mode))
