# SPDX-FileCopyrightText: 2021 Iwan Aucamp
#
# SPDX-License-Identifier: Apache-2.0

import logging
import os
import sys
import typing

import click

# import conans
import yaml
from conans import ConanFile  # type: ignore[import]
from conans.client.conan_api import Conan, ProfileData  # type: ignore[import]
from conans.client.graph.graph import Node  # type: ignore[import]
from conans.client.graph.printer import print_graph  # type: ignore[import]
from conans.client.installer import BinaryInstaller  # type: ignore[import]
from conans.client.output import ConanOutput  # type: ignore[import]
from conans.client.recorder.action_recorder import (  # type: ignore[import]
    ActionRecorder,
)

from .cli_lenv import cli_lenv
from .cli_senv import cli_senv
from .helpers import ConanFileHelper, EnvHelper
from .utils import shlex_join

LOGGER = logging.getLogger(__name__)


"""
https://click.palletsprojects.com/en/7.x/api/#parameters
https://click.palletsprojects.com/en/7.x/options/
https://click.palletsprojects.com/en/7.x/arguments/
"""


@click.group()
@click.option("-v", "--verbose", "verbosity", count=True)
@click.version_option(version="0.0.0")
@click.pass_context
def cli(ctx: click.Context, verbosity: int) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    ctx.ensure_object(dict)

    if verbosity is not None:
        root_logger = logging.getLogger("")
        root_logger.propagate = True
        new_level = root_logger.getEffectiveLevel()
        for _index in range(verbosity):
            new_level -= 10 if new_level > logging.DEBUG else 1
        root_logger.setLevel(max(0, new_level))

    LOGGER.debug(
        "ctx = %s, logging.level = %s, LOGGER.level = %s",
        ctx,
        logging.getLogger("").getEffectiveLevel(),
        LOGGER.getEffectiveLevel(),
    )


SingularCollection = typing.Union[typing.Sequence, typing.Set]


MutableSingularCollection = typing.Union[typing.MutableSequence, typing.MutableSet]
MutableSingularCollectionT = typing.TypeVar(
    "MutableSingularCollectionT", bound=MutableSingularCollection
)


def extend(
    dest: MutableSingularCollectionT, source: SingularCollection
) -> MutableSingularCollectionT:
    for item in source:
        dest.add(item)
    return dest


@cli.command(name="exec")
@click.option("--options-obj", "--oo", "options_object", type=str)
@click.option("--settings-obj", "--so", "settings_object", type=str)
@click.argument("reference", nargs=1, type=str)
@click.argument("args", nargs=-1)
@click.pass_context
def cli_exec(
    ctx: click.Context,
    options_object: typing.Optional[str],
    settings_object: typing.Optional[str],
    reference: str,
    args: typing.List[str],
) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    settings = None if settings_object is None else yaml.safe_load(settings_object)
    options = None if options_object is None else yaml.safe_load(options_object)

    build = None
    update = False
    remote_name = None
    profile_name = None
    profile_build = None
    install_folder = None
    options = None
    env = None
    lockfile = None
    recorder = ActionRecorder()

    profile_host = ProfileData(
        profiles=profile_name, settings=settings, options=options, env=env
    )

    (api, cache, user_io) = Conan.factory()
    api.create_app()

    logging.debug("will get info ...")

    reference, graph_info = api._info_args(
        reference, install_folder, profile_host, profile_build, lockfile=lockfile
    )
    remotes = api.app.load_remotes(remote_name=remote_name, update=update)
    logging.debug("remotes = %s", remotes)
    deps_graph = api.app.graph_manager.load_graph(
        reference, None, graph_info, build, update, False, remotes, recorder
    )
    logging.debug("(%s)deps_graph = %s", type(deps_graph), deps_graph)
    output = ConanOutput(sys.stderr, color=True)

    logging.debug("will print_graph ...")
    print_graph(deps_graph, output)
    nodes_by_level = deps_graph.by_levels()
    logging.debug("nodes_by_level = %s", nodes_by_level)
    root_node: Node = nodes_by_level[-1][0]
    logging.debug("(%s)root_node = %s", type(root_node), root_node)
    actual_node: Node = nodes_by_level[-2][0]
    logging.debug("(%s)actual_node = %s", type(actual_node), actual_node)

    logging.debug("nodes_by_level = %s", nodes_by_level)

    conanfile: ConanFile

    binary_installer = BinaryInstaller(api.app, recorder=recorder)
    binary_installer.install(
        deps_graph, remotes, build, update, keep_build=False, graph_info=graph_info
    )

    logging.debug("nodes_by_level = %s", nodes_by_level)

    for level_index, level in enumerate(nodes_by_level):
        logging.debug("nodes_by_level[%s] = %s", level_index, level)
        for node_index, node in enumerate(level):
            logging.debug("nodes_by_level[%s][%s] = %s", level_index, node_index, node)
            conanfile = node.conanfile
            logging.debug("(%s)conanfile = %s", type(conanfile), conanfile)
            logging.debug(
                "isinstance(conanfile, ConanFile) = %s",
                isinstance(conanfile, ConanFile),
            )
            for attr in {
                "package_folder",
                "install_folder",
                "build_folder",
                "source_folder",
            }:
                logging.debug("conanfile.%s = %s", attr, getattr(conanfile, attr))
            logging.debug("(%s)conanfile.env = %s", type(conanfile.env), conanfile.env)

    conanfile = root_node.conanfile

    # extra_envs: typing.Dict[str, typing.Union[str, typing.List[str]]] = {}

    extra_envs = ConanFileHelper.conanfile_extra_envs(conanfile)

    logging.info("extra_envs = %s", extra_envs)

    new_environ = os.environ.copy()
    EnvHelper.updat_environ(new_environ, extra_envs)

    logging.debug("will run %s", shlex_join(args))
    logging.debug("extra_envs = %s", extra_envs)
    logging.debug("new_environ['PATH'] = %s", new_environ["PATH"])

    os.execvpe(args[0], args, env=new_environ)


def main() -> None:
    logging.basicConfig(
        level=os.environ.get("PYTHON_LOGGING_LEVEL", logging.WARN),
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=(
            "%(asctime)s.%(msecs)03d %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        ),
    )
    cli.add_command(cli_lenv)
    cli.add_command(cli_senv)
    cli(obj={})


if __name__ == "__main__":
    main()
