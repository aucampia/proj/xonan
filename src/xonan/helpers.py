# SPDX-FileCopyrightText: 2021 Iwan Aucamp
#
# SPDX-License-Identifier: Apache-2.0

import collections.abc
import enum
import json
import logging
import os
import pathlib
import shlex
import subprocess
import sys
import typing

from conans import ConanFile  # type: ignore[import]
from xdg import xdg_config_home, xdg_data_home

from .types import EnvironT, ExtraEnvMapping, ExtraEnvMutableMapping
from .utils import coelasece_safe, shlex_join

LOGGER = logging.getLogger(__name__)


class EnvHelper:
    @classmethod
    def updat_environ(cls, environ: EnvironT, extra_envs: ExtraEnvMapping) -> EnvironT:
        for env_name, env_value in extra_envs.items():
            if not isinstance(env_value, str) and isinstance(
                env_value, (collections.abc.Set, collections.abc.Sequence)
            ):
                new_value = os.pathsep.join(env_value)
                if env_name in environ:
                    new_value = new_value + os.pathsep + environ[env_name]
                environ[env_name] = new_value
            else:
                assert isinstance(env_value, str)
                environ[env_name] = env_value
        return environ

    @classmethod
    def make_shenv(cls, extra_envs: ExtraEnvMapping) -> str:
        lines: typing.List[str] = []
        for env_name, env_value in extra_envs.items():
            if not isinstance(env_value, str) and isinstance(
                env_value, (collections.abc.Set, collections.abc.Sequence)
            ):
                extras = shlex.quote(os.pathsep).join(
                    [shlex.quote(item) for item in env_value]
                )
                current = f'''"${{{env_name}:+{os.pathsep}${{{env_name}}}}}"'''
                lines.append(f"""{env_name}={extras}{current}""")
            else:
                assert isinstance(env_value, str)
                lines.append(f"""{env_name}={shlex.quote(env_value)}""")
            lines.append(f"export {env_name}")
        lines.append("")
        return os.linesep.join(lines)


class ConanFileHelper:
    conanfile: ConanFile

    def __init__(self, conanfile: ConanFile):
        self.conanfile = conanfile

    @classmethod
    def conanfile_extra_envs(
        cls,
        conanfile: ConanFile,
        output: typing.Optional[ExtraEnvMutableMapping] = None,
    ) -> ExtraEnvMapping:
        extra_envs = output if output is not None else {}

        for dep in conanfile.deps_cpp_info.deps:
            lib_paths = extra_envs.setdefault("LD_LIBRARY_PATH", [])
            assert isinstance(lib_paths, list)
            lib_paths.extend(conanfile.deps_cpp_info[dep].lib_paths)

            lib_paths = extra_envs.setdefault("DYLD_LIBRARY_PATH", [])
            assert isinstance(lib_paths, list)
            lib_paths.extend(conanfile.deps_cpp_info[dep].lib_paths)

            bin_paths = extra_envs.setdefault("PATH", [])
            assert isinstance(bin_paths, list)
            bin_paths.extend(conanfile.deps_cpp_info[dep].bin_paths)

        for dep in conanfile.deps_env_info.deps:
            vars = conanfile.deps_env_info[dep].vars
            for var_key, var_value in vars.items():
                if not isinstance(var_value, str) and isinstance(
                    var_value, (collections.abc.Set, collections.abc.Sequence)
                ):
                    collection = extra_envs.setdefault(var_key, [])
                    assert isinstance(collection, list)
                    collection.extend(var_value)
                else:
                    extra_envs[var_key] = var_value

        return extra_envs

    @classmethod
    def buildinfo_extra_envs(
        cls,
        buildinfo: typing.Dict[str, typing.Any],
        output: typing.Optional[ExtraEnvMutableMapping] = None,
    ) -> ExtraEnvMapping:
        extra_envs = output if output is not None else {}

        dep_mappings = [
            {"src": ["lib_paths"], "dst": ["DYLD_LIBRARY_PATH", "LD_LIBRARY_PATH"]},
            {"src": ["bin_paths"], "dst": ["PATH"]},
        ]

        dependencies = typing.cast(
            typing.List[typing.Dict[str, typing.Any]], buildinfo["dependencies"]
        )
        for depdendency in dependencies:
            for dep_mapping in dep_mappings:
                for src in dep_mapping["src"]:
                    src_list = depdendency[src]
                    for dst in dep_mapping["dst"]:
                        for src_list_item in src_list:
                            dest = extra_envs.setdefault(dst, [])
                            assert isinstance(dest, list)
                            dest.append(src_list_item)

        deps_env_info = typing.cast(
            typing.Dict[str, typing.Union[str, typing.List[str]]],
            buildinfo["deps_env_info"],
        )

        for deps_env_name, deps_env_value in deps_env_info.items():
            if isinstance(
                deps_env_value, collections.abc.Collection
            ) and not isinstance(deps_env_value, str):
                for deps_env_value_item in deps_env_value:
                    dest = extra_envs.setdefault(deps_env_name, [])
                    assert isinstance(dest, list)
                    dest.append(deps_env_value_item)
            else:
                extra_envs[deps_env_name] = deps_env_value

        return extra_envs


class ConanEnvHelper:
    conan_path: pathlib.Path
    env_path: pathlib.Path
    buildinfo_json_path: pathlib.Path

    def __init__(self, conan_path: pathlib.Path, env_path: pathlib.Path) -> None:
        self.conan_path = conan_path
        self.env_path = env_path
        self.buildinfo_json_path = self.env_path / "conanbuildinfo.json"


class InstallMode(enum.Enum):
    NEVER = "never"
    NEEDED = "needed"
    ALWAYS = "always"


class ContextHelper:
    env_slug: str = ".xonan-env"
    conan_path: pathlib.Path
    env_path: pathlib.Path
    buildinfo_json_slug: str = "conanbuildinfo.json"
    buildinfo_json_path: pathlib.Path
    conanfile: pathlib.Path

    def __init__(
        self,
        conan_path: pathlib.Path,
        env_path: typing.Optional[pathlib.Path] = None,
        env_slug: typing.Optional[str] = None,
        buildinfo_json_path: typing.Optional[pathlib.Path] = None,
        buildinfo_json_slug: typing.Optional[str] = None,
    ) -> None:
        # self.conan_path = pathlib.Path.cwd()
        self.conan_path = conan_path
        if env_slug:
            self.env_slug = env_slug
        self.env_path = coelasece_safe(
            env_path, fallback=self.conan_path / self.env_slug
        )
        if buildinfo_json_slug:
            self.buildinfo_json_slug = buildinfo_json_slug
        self.buildinfo_json_path = coelasece_safe(
            buildinfo_json_path, fallback=self.env_path / self.buildinfo_json_slug
        )
        self.lockfile = self.conan_path / "conan.lock"
        self.conanfile = self.conan_path / "conanfile.txt"
        self.shenvfile = self.conan_path / "env.sh"

    def lock_create(self, to_stderr: bool = False, if_missing: bool = True) -> None:
        if self.lockfile.exists() and not if_missing:
            LOGGER.debug("not creating lock as it already exists.")
            return
        args = [
            "conan",
            "lock",
            "create",
            f"--lockfile-out={self.lockfile}",
            f"{self.conanfile}",
        ]
        LOGGER.debug("(cwd=%s) will run %s", os.getcwd(), shlex_join(args))
        subprocess.run(args, check=True, stdout=sys.stderr if to_stderr else None)

    def _install(self, to_stderr: bool = False) -> None:
        self.lock_create(to_stderr=to_stderr)
        args = [
            "conan",
            "install",
            "--generator=json",
            "--build=missing",
            f"--install-folder={self.env_path}",
            f"--lockfile={self.conan_path}",
            f"{self.conan_path}",
        ]
        LOGGER.debug("(cwd=%s) will run %s", os.getcwd(), shlex_join(args))
        subprocess.run(args, check=True, stdout=sys.stderr if to_stderr else None)

    def install(self, to_stderr: bool = False) -> None:
        self._install(to_stderr=to_stderr)
        self.write_shenv(path=self.shenvfile, install_mode=InstallMode.NEVER)

    def extra_envs(self, install_mode: InstallMode) -> ExtraEnvMapping:
        if (install_mode is InstallMode.ALWAYS) or (
            install_mode is InstallMode.NEEDED and not self.buildinfo_json_path.exists()
        ):
            self._install(True)

        with self.buildinfo_json_path.open("r") as fileptr:
            buildinfo = typing.cast(typing.Dict[str, typing.Any], json.load(fileptr))

        return ConanFileHelper.buildinfo_extra_envs(buildinfo)

    def exec(self, install_mode: InstallMode, args: typing.List[str]) -> None:

        extra_envs = self.extra_envs(install_mode=install_mode)
        new_environ = os.environ.copy()
        EnvHelper.updat_environ(new_environ, extra_envs)

        LOGGER.debug("will run %s", shlex_join(args))
        LOGGER.debug("extra_envs = %s", extra_envs)
        LOGGER.debug("new_environ['PATH'] = %s", new_environ["PATH"])

        os.execvpe(args[0], args, env=new_environ)

    def write_shenv(self, path: pathlib.Path, install_mode: InstallMode) -> None:
        extra_envs = self.extra_envs(install_mode=install_mode)
        shenv = EnvHelper.make_shenv(extra_envs)
        path.parent.mkdir(parents=True, exist_ok=True)
        path.write_text(shenv)
        return

    def show_shenv(self, install_mode: InstallMode) -> None:
        extra_envs = self.extra_envs(install_mode=install_mode)
        shenv = EnvHelper.make_shenv(extra_envs)
        sys.stdout.write(shenv)
        return


class DirHelper:
    app_slug: str
    _data: pathlib.Path
    _config: pathlib.Path

    def __init__(self, app_slug: str = "xonan-1.0"):
        self.app_slug = app_slug
        self._data = xdg_data_home() / self.app_slug
        self._config = xdg_config_home() / self.app_slug

    @property
    def data(self) -> pathlib.Path:
        return self._data

    @property
    def config(self) -> pathlib.Path:
        return self._config


dir_helper = DirHelper()


class SEnvHelper:
    name: str
    conanfile: pathlib.Path
    config_dir: pathlib.Path
    context_helper: ContextHelper

    def __init__(self, name: str):
        self.name = name
        self.config_dir = dir_helper.config / self.name
        self.conanfile = self.config_dir / "conanfile.txt"
        self.context_helper = ContextHelper(self.config_dir)
