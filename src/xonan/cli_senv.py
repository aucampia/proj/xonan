# SPDX-FileCopyrightText: 2021 Iwan Aucamp
#
# SPDX-License-Identifier: Apache-2.0

import json
import logging
import sys
import typing

import click

# from .cli import cli
from .helpers import InstallMode, SEnvHelper

LOGGER = logging.getLogger(__name__)


@click.group(name="senv")
@click.option(
    "--name",
    "-n",
    type=str,
    required=True,
    help="The user name ...",
)
@click.pass_context
def cli_senv(ctx: click.Context, name: str) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )


@cli_senv.command(name="info")
@click.pass_context
def cli_senv_info(ctx: click.Context) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    assert ctx.parent
    name: str = ctx.parent.params["name"]
    env = SEnvHelper(name)
    json.dump(
        {
            "conanfile": str(env.conanfile),
            "config_dir": str(env.config_dir),
        },
        sys.stdout,
        indent=2,
    )
    sys.stdout.write("\n")


@cli_senv.command(name="edit")
@click.pass_context
def cli_senv_edit(ctx: click.Context) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    assert ctx.parent
    name: str = ctx.parent.params["name"]
    env = SEnvHelper(name)
    env.conanfile.parent.mkdir(parents=True, exist_ok=True)
    click.edit(filename=str(env.conanfile))


@cli_senv.command(name="install")
@click.pass_context
def cli_senv_install(ctx: click.Context) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    assert ctx.parent
    name: str = ctx.parent.params["name"]
    env = SEnvHelper(name)
    env.context_helper.install()


@cli_senv.command(name="exec")
@click.option("--install-mode", "-i", type=click.Choice(e.value for e in InstallMode))
@click.argument("args", nargs=-1)
@click.pass_context
def cli_senv_exec(
    ctx: click.Context,
    install_mode: str,
    args: typing.List[str],
) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    assert ctx.parent
    name: str = ctx.parent.params["name"]
    env = SEnvHelper(name)
    env.context_helper.exec(install_mode=InstallMode(install_mode), args=args)


@cli_senv.command(name="env")
@click.option("--install-mode", "-i", type=click.Choice(e.value for e in InstallMode))
@click.argument("args", nargs=-1)
@click.pass_context
def cli_senv_env(
    ctx: click.Context,
    install_mode: str,
) -> None:
    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    assert ctx.parent
    name: str = ctx.parent.params["name"]
    env = SEnvHelper(name)
    env.context_helper.show_shenv(install_mode=InstallMode(install_mode))
    LOGGER.debug("done")
