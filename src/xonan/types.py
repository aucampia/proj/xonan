# SPDX-FileCopyrightText: 2021 Iwan Aucamp
#
# SPDX-License-Identifier: Apache-2.0

import typing

Environ = typing.MutableMapping[str, str]
ExtraEnvMapping = typing.Mapping[str, typing.Union[str, typing.Collection[str]]]
ExtraEnvMutableMapping = typing.MutableMapping[
    str, typing.Union[str, typing.Collection[str]]
]

EnvironT = typing.TypeVar("EnvironT", bound=Environ)
