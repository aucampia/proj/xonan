# SPDX-FileCopyrightText: 2021 Iwan Aucamp
#
# SPDX-License-Identifier: Apache-2.0

import inspect
import shlex
import typing

GenericT = typing.TypeVar("GenericT")


def coalesce(*args: typing.Optional[GenericT]) -> typing.Optional[GenericT]:
    for arg in args:
        if arg is not None:
            return arg
    return None


def coelasece_safe(*args: typing.Optional[GenericT], fallback: GenericT) -> GenericT:
    for arg in args:
        if arg is not None:
            return arg
    return fallback


def vdict(*keys: str, obj: typing.Any = None) -> typing.Dict[str, typing.Any]:
    if obj is None:
        lvars = typing.cast(typing.Any, inspect.currentframe()).f_back.f_locals
        return {key: lvars[key] for key in keys}
    return {key: getattr(obj, key, None) for key in keys}


# https://github.com/python/cpython/blob/master/Lib/shlex.py#L318
def shlex_join(split_command: typing.List[str]) -> str:
    return " ".join(shlex.quote(arg) for arg in split_command)
