# ...

TODO:

* lockfiles
* global vs local move
* add deps via command line
* path to conanfile
* Source exports
* auto install missing
* ...
* conanfiles from github?
* global source
* Never install flag

## ...

```
https://docs.conan.io/en/latest/reference/conanfile_txt.html

xonan uenv -n global info
xonan uenv -n global edit
xonan uenv -n global exec -i -- openssl version
```

## ...

```
mkdir -vp /var/tmp/xonan-demo
cd /var/tmp/xonan-demo
cat > conanfile.txt <<EOF
[requires]
openssl/1.1.1h
EOF

xonan env install
exec xonan env exec -- bash
xonan env exec -- which openssl
xonan env exec -- openssl version
```

```
conan search -r localhost

xonan -vvvv exec openssl/1.1.1h@ -- 1 2 3
xonan -vvvv exec libcurl/7.74.0@ -- 1 2 3
xonan -vvvv exec gradle-prebuilt/6.7@aucampia/default -- 1 2 3
xonan -vvvv exec kotlin-prebuilt/1.4.10@aucampia/default -- kotlin -version
xonan exec kotlin-prebuilt/1.4.10@aucampia/default -- kotlin -version
xonan exec kotlin-prebuilt/1.4.10@aucampia/default -- env | grep .conan/data
xonan exec cmake/3.19.4@ -- cmake --version


xonan env install
xonan env exec -- bash
xonan env exec --reinstall -- bash
```

```
xonan nenv -n
```

```
cwd env
named env
xonan env-cwd
xonan env-named
xonan env-global
```


## ...

```
~/d.sws/conan-envs
```

## Resources

* https://github.com/conan-io/conan/blob/develop/conans/client/generators/virtualrunenv.py
* https://github.com/conan-io/conan/blob/develop/conans/client/generators/virtualbuildenv.py
* https://github.com/conan-io/conan/blob/develop/conans/client/generators/virtualenv.py
* https://github.com/conan-io/conan/blob/develop/conans/client/envvars/environment.py
